import React from 'react';
import PropTypes from 'prop-types';
import * as rc from 'recompose';

const enhance = rc.compose(
  rc.setPropTypes({
    name: PropTypes.string,
    power: PropTypes.number,
    fuelConsumption: PropTypes.number,
    onSubmit: PropTypes.func
  })
);

class EditCarForm extends React.Component {
  state = {
    name: this.props.name,
    power: this.props.power,
    fuelConsumption: this.props.fuelConsumption
  };

  onChange = field => e => {
    this.setState({
      [field]: e.target.value
    });
  };

  onSubmit = e => {
    e.preventDefault();
    const { onSubmit } = this.props;
    if (onSubmit) {
      onSubmit(this.state);
    }
  };
  render() {
    const { name, power, fuelConsumption } = this.state;
    return (
      <form onSubmit={this.onSubmit}>
        <h3>Add car</h3>
        <div>
          <label>
            Name
            <input type="text" value={name} required onChange={this.onChange('name')}/>
          </label>
        </div>
        <div>
          <label>
            Power
            <input type="number" value={power} required onChange={this.onChange('power')}/>
          </label>
        </div>
        <div>
          <label>
            Fuel consumption
            <input type="number" value={fuelConsumption} required onChange={this.onChange('fuelConsumption')}/>
          </label>
        </div>
        <button>submit</button>
      </form>
    );
  }
}

export default enhance(EditCarForm);

import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import * as rc from 'recompose';

const enhance = rc.compose(
  rc.setPropTypes({
    cars: PropTypes.array,
  }),
);

const CarList = ({ cars }) => {
  return (
    <div>
      <ul>
        {cars && cars.length ? (
          cars.map(car => (
            <li>
              Name: {car.name} / power: {car.power} / fuel consumption: {car.fuelConsumption}
              <Link to={`/change/${car.name}`} className="action__link">
                Change
              </Link>
              <Link to={`/delete/${car.name}`} className="action__link">
                Delete
              </Link>
            </li>
          ))
        ) : (
          'Empty list...'
        )}
      </ul>
    </div>
  );
};

export default enhance(CarList);

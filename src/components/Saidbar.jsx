import React from 'react';
import { Link } from 'react-router-dom';

const Sidebar = ({ cars }) => {
  return (
    <div className="sidebar">
      <Link to="/list" className="sidebar__link">
        List
      </Link>
      <Link to="/add" className="sidebar__link">
        Add
      </Link>
    </div>
  );
};

export default Sidebar;

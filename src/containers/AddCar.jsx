import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { actions } from '../state/actions.js';

import { bindActionCreators } from 'redux';
import EditCarForm from '../components/EditCarForm';
import { history } from '../state/history';

class AddCarContainer extends React.Component {
  onSubmit = car => {
    this.props.addCar(car);
    this.context.router.history.push('/');
  };

  render() {
    const { cars } = this.props;
    return (
      <div>
        <Link to="/">Back</Link>
        <EditCarForm onSubmit={this.onSubmit} />
      </div>
    );
  }
  static contextTypes = {
    router: PropTypes.object.isRequired
  };
}

function mapStateToProps(state) {
  return {
    cars: state.cars
  };
}

function mapDispatchToProps(dispatch) {
  return {
    addCar: bindActionCreators(actions.addCar, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AddCarContainer);

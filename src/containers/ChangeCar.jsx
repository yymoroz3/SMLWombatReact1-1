import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';
import { actions } from '../state/actions';
import { carSelector } from '../state/selectors';

import { bindActionCreators } from 'redux';
import EditCarForm from '../components/EditCarForm';
import { history } from '../state/history';

class ChangeCar extends React.Component {
  onSubmit = car => {
    this.props.changeCar(car);
    this.context.router.history.push('/');
  };

  render() {
    const { cars, match } = this.props;
    const { name } = match.params;
    const car = carSelector.getCarByName(cars, name);
    console.log(car, name);
    if (!car) {
      return <Redirect to="/" />;
    }
    return (
      <div>
        <Link to="/">Back</Link>
        <EditCarForm onSubmit={this.onSubmit} {...car} />
      </div>
    );
  }
  static contextTypes = {
    router: PropTypes.object.isRequired
  };
}

function mapStateToProps(state) {
  return {
    cars: state.cars
  };
}

function mapDispatchToProps(dispatch) {
  return {
    changeCar: bindActionCreators(actions.changeCar, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ChangeCar);

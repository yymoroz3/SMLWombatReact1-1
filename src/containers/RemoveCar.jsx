import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';
import { actions } from '../state/actions';
import { carSelector } from '../state/selectors';

import { bindActionCreators } from 'redux';
import EditCarForm from '../components/EditCarForm';
import { history } from '../state/history';

class RemoveCar extends React.Component {
  onSubmit = car => {
    this.props.changeCar(car);
    this.context.router.history.push('/');
  };

  onYes = () => {
    const car = this.getCar();
    this.props.removeCar(car);
    this.context.router.history.push('/');
  };

  onNo = () => {
    this.context.router.history.push('/');
  };

  getCar() {
    const { cars, match } = this.props;
    const { name } = match.params;
    const car = carSelector.getCarByName(cars, name);
    return car;
  }

  render() {
    const car = this.getCar();
    if (!car) {
      return <Redirect to="/" />;
    }
    return (
      <div>
        <h4>
          Delete car: {car.name} ?
        </h4>
        <div>
          <button onClick={this.onYes}>yes</button>
          <button onClick={this.onNo}>no</button>
        </div>
      </div>
    );
  }
  static contextTypes = {
    router: PropTypes.object.isRequired
  };
}

function mapStateToProps(state) {
  return {
    cars: state.cars
  };
}

function mapDispatchToProps(dispatch) {
  return {
    removeCar: bindActionCreators(actions.removeCar, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(RemoveCar);

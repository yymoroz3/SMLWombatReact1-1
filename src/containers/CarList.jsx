import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import CarList from '../components/CarList';

class CarListContainer extends React.Component {
  render() {
    const { cars } = this.props;
    console.log(cars);
    return <CarList cars={cars} />;
  }
}

function mapStateToProps(state) {
  return {
    cars: state.cars
  };
}

export default connect(mapStateToProps)(CarListContainer);

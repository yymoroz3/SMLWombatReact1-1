export const carSelector = {
  getCarByName(state, name) {
    const car = state.find(car => car.name === name);
    return car;
  }
};

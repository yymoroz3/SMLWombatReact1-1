export const actionType = {
  ADD_CAR: 'ADD_CAR',
  REMOVE_CAR: 'REMOVE_CAR',
  CHANGE_CAR: 'CHANGE_cAR',
  VIEW_CAR: 'VIEW_CAR'
};

export const actions = {
  addCar(car) {
    return {
      type: actionType.ADD_CAR,
      payload: car
    };
  },
  changeCar(car) {
    return {
      type: actionType.CHANGE_CAR,
      payload: car
    };
  },
  removeCar(car) {
    return {
      type: actionType.REMOVE_CAR,
      payload: car
    };
  },
};

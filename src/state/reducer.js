import { combineReducers } from 'redux';

import { actionType } from './actions';

var initSate = [];
/*
carObj: 
    {
      name: string,
      power: number,
      fuelConsumption: number,
    }
  
  */

const addCar = (state, payload) => {
  const newCars = [...state];
  newCars.push(payload);
  return newCars;
};

const changeCar = (state, payload) => {
  const newCartList = state.filter(car => car.name !== payload.name);
  newCartList.push(payload);
  return newCartList;
};

const removeCar = (state, payload) => {
  return state.filter(car => car.name !== payload.name);
};

const carReducer = (state = initSate, { type, payload }) => {
  switch (type) {
    case actionType.ADD_CAR: {
      return addCar(state, payload);
    }
    case actionType.CHANGE_CAR: {
      return changeCar(state, payload);
    }
    case actionType.REMOVE_CAR: {
      return removeCar(state, payload);
    }
  }

  return state;
};

export default combineReducers({
  cars: carReducer
});

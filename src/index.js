import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route, Switch, Redirect, withRouter } from 'react-router-dom';
import { store } from './state/store';
import { history } from './state/history';
import './index.css';
import CarList from './containers/CarList';
import AddCar from './containers/AddCar';
import ChangeCar from './containers/ChangeCar';
import RemoveCar from './containers/RemoveCar.jsx';
import registerServiceWorker from './registerServiceWorker';
import Saidbar from './components/Saidbar';
import { TransitionGroup, CSSTransition } from 'react-transition-group';

import 'animate.css';

const App = withRouter(({ location }) => (
  <div className="content">
    <Saidbar key="Saidbar" />
    <div className="main">
      <TransitionGroup>
        <CSSTransition
          key={location.key}
          timeout={1000}
          classNames={{
            enter: 'animated',
            enterActive: 'bounceInRight',
            exit: 'animated',
            exitActive: 'bounceOutRight',
          }}
        >
          <Switch location={location}>
            <Route exact path="/add" component={AddCar} />
            <Route exact path="/change/:name" component={ChangeCar} key="ChangeCar" />
            <Route exact component={RemoveCar} path="/delete/:name" />
            <Route exact component={CarList} path="/list" />
            <Route exact component={CarList} path="/" />
            <Route exact component={CarList} path="*" />
          </Switch>
        </CSSTransition>
      </TransitionGroup>
    </div>
  </div>
));

ReactDOM.render(
  <Provider store={store}>
    <Router history={history}>
      <App />
    </Router>
  </Provider>,
  document.getElementById('root'),
);
registerServiceWorker();

/* import React from 'react';
import { render } from 'react-dom';
import { TransitionGroup, CSSTransition } from 'react-transition-group';
import { BrowserRouter, Switch, Route, Link, withRouter } from 'react-router-dom';

const Home = () => <div className="home">You are on the home page</div>;

const Other = () => <div className="other">You are on the other page</div>;

const App = withRouter(({ location }) => (
  <div>
    <nav>
      <ul>
        <li>
          <Link to="/">Home</Link>
        </li>
        <li>
          <Link to="/other">Other</Link>
        </li>
      </ul>
    </nav>
    <TransitionGroup>
      <CSSTransition key={location.key} classNames="fade" timeout={1000}>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/other" component={Other} />
        </Switch>
      </CSSTransition>
    </TransitionGroup>
  </div>
));

render(
  <BrowserRouter>
    <App />
  </BrowserRouter>,
  document.getElementById('root'),
); */

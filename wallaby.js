module.exports = w => {
  return {
    files: ['src/**/*.js', '!src/**/*.test.ts'],
    tests: ['src/**/*.test.js'],
    debug: true,
    env: {
      type: 'node'
    }
  };
};
